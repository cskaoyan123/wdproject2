from django.db import models
from django.contrib.auth.models import User

# 返回问题
class Problem(models.Model):
    user=models.ForeignKey(User)
    # 对于一个问题,pub_date并不是首先就必须的，是在过滤的时候作为辅助过滤很有用
    pub_date=models.DateTimeField()
    title=models.CharField(max_length=200)
    body=models.TextField()

    #转中文
    def __unicode__(self):
        return self.title
