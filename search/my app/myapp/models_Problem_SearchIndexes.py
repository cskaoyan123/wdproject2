import datetime
from haystack import indexes
from myapp.models import Problem


class ProblemIndex(indexes.SearchIndex, indexes.Indexable):
    # document=True 表示Haystack和搜索引擎把text作为主要的检索字段
    # use_template=True 就可以用数据模版来构建文档的搜索引擎索引
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='user')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Problem

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())